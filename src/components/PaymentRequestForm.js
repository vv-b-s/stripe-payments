import React from "react";
import {injectStripe, PaymentRequestButtonElement} from "react-stripe-elements";
import PropTypes from "prop-types";

class PaymentRequestForm extends React.Component {
    constructor(props) {
        super(props);
        const {stripe, clientSecret, onPaymentComplete, amount, currency} = props;

        // For full documentation of the available paymentRequest options, see:
        // https://stripe.com/docs/stripe.js#the-payment-request-object
        const paymentRequest = props.stripe.paymentRequest({
            country: 'DE',
            currency: currency.toLowerCase(),
            total: {
                label: 'Demo total',
                amount: amount * 100,
            },
        });

        paymentRequest.on('paymentmethod', function(ev) {
            // Confirm the PaymentIntent without handling potential next actions (yet).
            stripe.confirmCardPayment(
                clientSecret,
                {payment_method: ev.paymentMethod.id},
                {handleActions: false}
            ).then(function (confirmResult) {
                console.log(confirmResult);
                if (confirmResult.error) {
                    // Report to the browser that the payment failed, prompting it to
                    // re-show the payment interface, or show an error message and close
                    // the payment interface.
                    console.log("Payment request fail at line 32");
                    ev.complete('fail');
                } else {
                    // Report to the browser that the confirmation was successful, prompting
                    // it to close the browser payment method collection interface.
                    ev.complete('success');
                    // Let Stripe.js handle the rest of the payment flow.
                    stripe.confirmCardPayment(clientSecret).then(function (result) {
                        console.log(result);
                        if (result.error) {
                            console.log("Payment request fail at line 42");
                            // The payment failed -- ask your customer for a new payment method.
                        } else {
                            // The payment has succeeded.
                            onPaymentComplete(result.paymentIntent.id);
                        }
                    });
                }
            });
        });

        paymentRequest.canMakePayment().then((result) => {
            this.setState({canMakePayment: !!result});
        });

        this.state = {
            canMakePayment: false,
            paymentRequest,
        };
    }

    render() {
        console.log("Rendering payment request btn");
        return this.state.canMakePayment ? (
            <PaymentRequestButtonElement
                paymentRequest={this.state.paymentRequest}
                className="PaymentRequestButton"
                style={{
                    // For more details on how to style the Payment Request Button, see:
                    // https://stripe.com/docs/elements/payment-request-button#styling-the-element
                    paymentRequestButton: {
                        theme: 'light',
                        height: '64px',
                    },
                }}
            />
        ) : null;
    }
}

PaymentRequestForm.propTypes = {
    clientSecret: PropTypes.string,
    onPaymentComplete: PropTypes.func,
    amount: PropTypes.number,
    currency: PropTypes.string
};

export default injectStripe(PaymentRequestForm);