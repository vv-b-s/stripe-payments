import React from 'react';
import {Elements} from 'react-stripe-elements';

import InjectedCheckoutForm from './CheckoutForm';
import InjectedPaymentRequest from './PaymentRequestForm';
import PropTypes from "prop-types";

class MyStoreCheckout extends React.Component {
    render() {
        return (
            <Elements>
                {
                    this.props.useBtn ?
                        <InjectedPaymentRequest clientSecret={this.props.clientSecret}
                                                onPaymentComplete={this.props.onPaymentComplete}
                                                amount={this.props.amount} currency={this.props.currency}/> :
                        <InjectedCheckoutForm clientSecret={this.props.clientSecret}
                                              onPaymentComplete={this.props.onPaymentComplete}/>
                }
            </Elements>
        );
    }
}

MyStoreCheckout.propTypes = {
    useBtn: PropTypes.bool,
    clientSecret: PropTypes.string,
    onPaymentComplete: PropTypes.func,
    amount: PropTypes.number,
    currency: PropTypes.string
};

export default MyStoreCheckout;