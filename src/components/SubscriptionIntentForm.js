import React from 'react';
import backend from "./backend";
import PropTypes from 'prop-types';

class SubscriptionIntentForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            amount: "",
            currency: "",
            planId: "",
            useBtn: false,
        }
    }

    onFieldChange = (e) => {
        e.preventDefault();
        let key = e.target.id;
        let value = e.target.value;
        let newState = {};
        newState[key] = value;
        this.setState(newState);
    };

    onFormSubmit = (e) => {
        e.preventDefault();
        console.log("Generating client intent...");

        backend.post("/payment/secret", {
            amount: this.state.amount,
            currency: this.state.currency,
            planId: this.state.planId
        }, {
            headers: {
                authorization: this.props.authorization
            }
        }).then(res => {
            console.log(`Intent successfully created with client secret: ${res.data}`);
            this.handleClientSecret(res.data);
        }).catch(err => {
            console.log("Client secret generation failed");
            console.log(err);
        });

    };

    handleSelectChange = e => {
        this.setState({
            useBtn: e.target.value
        })
    };


    handleClientSecret = (secret) => {
        this.props.onClientSecret(secret, this.state.amount, this.state.currency, this.state.useBtn);
    };

    render() {
        return (
            <form onSubmit={this.onFormSubmit}>
                <p>Use payment request button. (Set to true if testing Google/Apple pay. <span style={{color: "red"}}>Use phone browser and host on SSL!</span>)
                </p>
                <select id="useBtn" value={this.state.useBtn} onChange={this.handleSelectChange}>
                    <option value={true}>YES</option>
                    <option value={false}>NO</option>
                </select>
                <p>Following fields are optional</p>
                <input type="text" placeholder="Amount" id="amount" onChange={this.onFieldChange}
                       value={this.state.amount}/>
                <input type="text" placeholder="Currency" id="currency" onChange={this.onFieldChange}
                       value={this.state.currency}/>
                <input type="text" placeholder="Plan Id" id="planId" onChange={this.onFieldChange}
                       value={this.state.planId}/>
                <input type="submit" value="Submit"/>
            </form>
        );
    }
}

SubscriptionIntentForm.propTypes = {
    authorization: PropTypes.string,
    onClientSecret: PropTypes.func
};

export default SubscriptionIntentForm;