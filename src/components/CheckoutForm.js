import React from 'react';
import PropTypes from 'prop-types';
import {injectStripe} from 'react-stripe-elements';

import CardSection from './CardSection';

class CheckoutForm extends React.Component {
    constructor(props) {
        super(props);
    }

    handleSubmit = (ev) => {
        // We don't want to let default form submission happen here, which would refresh the page.
        ev.preventDefault();

        // You can also use handleCardPayment with the PaymentIntents API.
        // See our handleCardPayment documentation for more:
        // https://stripe.com/docs/stripe-js/reference#stripe-handle-card-payment
        this.props.stripe.handleCardPayment(this.props.clientSecret)
            .then(status => {
                console.log(status);
                this.props.onPaymentComplete(status.paymentIntent.id)
            });
    };

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <CardSection />
                <button>Confirm order</button>
            </form>
        );
    }
}

CheckoutForm.propTypes = {
    clientSecret: PropTypes.string,
    onPaymentComplete: PropTypes.func
};

export default injectStripe(CheckoutForm);