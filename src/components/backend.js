import axios from 'axios';

export default axios.create({
    baseURL: "/evcbackend/api"
})