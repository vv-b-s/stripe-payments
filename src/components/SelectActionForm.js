import React from 'react';
import PropTypes from 'prop-types';
import backend from "./backend";

class SelectActionForm extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            stripeApiKey: '',
            actionTypes: ['add_card', 'pay_subscription'],
            actionType: 'add_card'
        }
    }

    onFieldChange = e => {
        e.preventDefault();
        let key = e.target.id;
        let value = e.target.value;
        let newState = {};
        newState[key] = value;
        this.setState(newState);
    };

    onSelectChange = e => {
        e.preventDefault();
        this.setState({
            actionType: e.target.value
        })

    };

    onFormSubmit = e => {
        e.preventDefault();
        backend.post('/user/login', {
            email: this.state.username,
            password: this.state.password
        }).then(res => {
            let jwt = res.headers.authorization;
            this.props.onSelectActionSubmit(jwt, this.state.stripeApiKey, this.state.actionType);
        }).catch(err => console.log(err));
    };

    render() {
        return (
            <form onSubmit={this.onFormSubmit}>
                <input type="text" value={this.state.stripeApiKey} id="stripeApiKey" onChange={this.onFieldChange}
                       placeholder="Stripe API key"/>
                <input type="text" value={this.state.username} id="username" onChange={this.onFieldChange} placeholder="Username"/>
                <input type="password" value={this.state.password} id="password" onChange={this.onFieldChange}
                       placeholder="Password"/>
                <select name="action" id="actionType" value={this.state.actionType} onChange={this.onSelectChange}>
                    {this.state.actionTypes.map(at => <option value={at}>{at}</option>)}
                </select>
                <input type="submit" value="Submit"/>
            </form>
        )
    }

}

SelectActionForm.propTypes = {
    onSelectActionSubmit: PropTypes.func
};

export default SelectActionForm;