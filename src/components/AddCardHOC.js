import React from 'react';
import {Elements} from 'react-stripe-elements';
import PropTypes from "prop-types";
import InjectAddCard from './AddCard'


class AddCardHOC extends React.Component {
    render() {
        return (
            <Elements>
                <InjectAddCard clientSecret={this.props.clientSecret} onCardAdded={this.props.onCardAdded}/>
            </Elements>
        );
    }
}

AddCardHOC.propTypes = {
    onCardAdded: PropTypes.func,
    clientSecret: PropTypes.string
};

export default AddCardHOC;