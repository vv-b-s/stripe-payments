import React from 'react';
import {StripeProvider} from 'react-stripe-elements';
import backend from "./components/backend";

import MyStoreCheckout from './components/MyStoreCheckout';
import SelectActionForm from "./components/SelectActionForm";
import SubscriptionIntentForm from "./components/SubscriptionIntentForm";
import AddCardHOC from "./components/AddCardHOC";

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            actionType: "",
            apiKey: "",
            authorization: "",
            clientSecret: "",
            amount: 0.0,
            currency: "",
            useBtn: false,
            addCardContent: <div></div>
        };
    }

    handleClientSecret = (clientSecret, amount, currency, useBtn) => {
        this.setState({
            clientSecret: clientSecret,
            amount: amount,
            currency: currency,
            useBtn: useBtn
        });
    };

    handleCardAdded = (paymentMethod) => {
        backend.post('/card', paymentMethod, {
            headers:{
                "Content-Type": 'text/plain',
                authorization: this.state.authorization
            }
        }).then(res => alert('success'));
    };

    handlePaymentComplete = (paymentIntentId) => {
        backend.post('/payment', {
            paymentIntentId: paymentIntentId,
            amount: this.state.amount
        }, {
            headers: {
                authorization: this.state.authorization
            }
        }).then(res => alert(res.data));
    };

    handleSelectActionSubmit = (jwt, apiKey, actionType) => {
        console.log(`Received API test data:\njwt: ${jwt}\napiKey: ${apiKey}\nactionType: ${actionType}`);

        this.setState({
            actionType: actionType,
            apiKey: apiKey,
            authorization: jwt,
        })
    };

    render() {
        switch (this.state.actionType) {
            case "add_card":
                backend.post('/card/secret',null, {
                    headers:{
                        authorization: this.state.authorization
                    }
                }).then(res => {
                    this.setState({
                        clientSecret: res.data,
                        addCardContent: (
                            <div>
                                <StripeProvider apiKey={this.state.apiKey}>
                                    <AddCardHOC onCardAdded={this.handleCardAdded} clientSecret={res.data}/>
                                </StripeProvider>
                            </div>
                        )
                    })
                });

                return this.state.addCardContent;
            case 'pay_subscription':
                return this.state.clientSecret === "" ?
                    (
                        <div>
                            <SubscriptionIntentForm onClientSecret={this.handleClientSecret}
                                                    authorization={this.state.authorization}/>
                        </div>
                    ) :
                    (
                        <div>
                            <StripeProvider apiKey={this.state.apiKey}>
                                <MyStoreCheckout clientSecret={this.state.clientSecret} useBtn={this.state.useBtn}
                                                 amount={this.state.amount} currency={this.state.currency}
                                                 onPaymentComplete={this.handlePaymentComplete}/>
                            </StripeProvider>
                        </div>
                    );
            default:
                return (
                    <div>
                        <SelectActionForm onSelectActionSubmit={this.handleSelectActionSubmit}/>
                    </div>
                );
        }
    }
}

export default App;