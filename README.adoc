= React application for testing Stripe API

== Installation

=== Configuring SSL Features

In order to test the https://stripe.com/docs/stripe-js/elements/payment-request-button[Payment Request Button] you would
need to host your application with a secured **https** connection.
You can either manually set up https certificates or use proxies like https://ngrok.io[ngrok] or https://serveo.net[serveo]
which will create the required infrastructure for you.

[CAUTION]
===============================
Currently there is an ongoing issue with hosting react applications through proxies.
This will display an error like the one below:

----
SecurityError: Failed to construct 'WebSocket': An insecure WebSocket connection may not be initiated from a page loaded over HTTPS.
----

To prevent this from happening you can https://github.com/facebook/create-react-app/pull/8079[set the following value in]  `node_modules/react-dev-utils/webpackHotDevClient.js`:

[source, javascript]
protocol: window.location.protocol === 'https:' ? 'wss' : 'ws',

===============================